package com.sample;

public class Sport {
	private SportName sportName;
	private int matchCount;
	private boolean criticalMiss;
	
	public boolean isCriticalMiss() {
		return criticalMiss;
	}

	public Sport(SportName sportName){
		this.sportName = sportName;
		this.matchCount = 0;
		this.criticalMiss = false;
	}
	
	public void increaseMatchCount(){
		this.matchCount = this.matchCount+1;
	}

	public SportName getSportName() {
		return sportName;
	}

	public int getMatchCount() {
		return matchCount;
	}
	
	public void setCriticalMiss(){
		this.criticalMiss = true;
	}
}
