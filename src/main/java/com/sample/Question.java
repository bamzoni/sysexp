package com.sample;

public class Question {
	
	private boolean asked;
	private QuestionType questionType;
	private Object[] answers;
	private boolean contextual;
	private Answer chosenAnswer;
	
	
	public Question(QuestionType questionType, Object[] answers, boolean contextual){
		this.questionType = questionType;
		this.answers=answers;
		this.asked = false;
		this.contextual = contextual;
	}
	
	
	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public Object[] getAnswers() {
		return answers;
	}

	public void setAnswers(Object[] answers) {
		this.answers = answers;
	}


	public Answer getChosenAnswer() {
		return chosenAnswer;
	}

	public void setChosenAnswer(Answer chosenAnswer) {
		this.chosenAnswer = chosenAnswer;
	}

	public QuestionType getQuestionType() {
		
		return questionType;
	}

	public boolean isAsked() {
		return asked;
	}

	public void setAsked() {
		this.asked = true;
	}


	public boolean isContextual() {
		return contextual;
	}


	public void setContextual(boolean contextual) {
		this.contextual = contextual;
	}

	
	
	
	
}
