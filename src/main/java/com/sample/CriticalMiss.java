package com.sample;

public class CriticalMiss {

	private SportName sportName;
	private Answer source;
	
	
	public CriticalMiss(SportName sport,  Answer source){
		this.sportName = sport;
		this.source = source;
	}

	public SportName getSportName() {
		return sportName;
	}
	
	public Answer getSource() {
		return source;
	}

}
