package com.sample;

public enum SportName {
soccer,
football,
basketball,
skiing,
iceskating,
volleyball,
running,
bowling,
futsal,
judo,
boxing,
tennis,
curling,
kickboxing,
jujitsu,
biathlon,
shootingsport,
hockey,
cycling,
archery,
darts;

}