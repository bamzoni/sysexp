package com.sample;

import java.util.HashMap;

public class QuestionTextBase {
	public static final HashMap<QuestionType, String> questionBase = new HashMap();
	
	public static final HashMap<SportName, String> sportBase = new HashMap();
	
	public static void initQuestionTextBase(){
		questionBase.put(QuestionType.outdoors, "Should the sport be done outdoors or indoors?");
		questionBase.put(QuestionType.winter, "Is it a winter sport?");
		questionBase.put(QuestionType.stamina, "Is based on stamina?");
		questionBase.put(QuestionType.team, "Should it be a team sport?");
		questionBase.put(QuestionType.snow, "Should it be done on snow or on ice?");
		questionBase.put(QuestionType.directcompetition, "Should it be based on direct competition other person/people?");
		questionBase.put(QuestionType.sizeofteam, "How big the team should be?");
		questionBase.put(QuestionType.physicalcontact, "Should it allow physical contact between opposing participants?");
		questionBase.put(QuestionType.martialart, "Should it be a martial art?");
		questionBase.put(QuestionType.martialartstyle, "Should that be a striking or grappling oriented martial art?");
		questionBase.put(QuestionType.martialartlegs, "Should striking with feet be allowed?");
		questionBase.put(QuestionType.ball, "Should the sport revolve around a ball?");
		questionBase.put(QuestionType.mixedpairs, "Should mixed gender pairs be allowed");
		questionBase.put(QuestionType.technical, "Should it be a technical sport?");
		questionBase.put(QuestionType.target, "Should the sport revolve around hitting the target?");
		questionBase.put(QuestionType.specialequipment, "Should it require special equipment?");
		questionBase.put(QuestionType.timed, "Should the result be determined by time?");
		questionBase.put(QuestionType.score, "Should the result be determined by score?");
		questionBase.put(QuestionType.targetequipment, "What should be the equipment?");
		questionBase.put(QuestionType.ballhandsfeet, "Should the sport be done using hands or feet?");
		
		sportBase.put(SportName.soccer, "Soccer");
		sportBase.put(SportName.football, "Football");
		sportBase.put(SportName.basketball, "Basketball");
		sportBase.put(SportName.skiing, "Skiing");
		sportBase.put(SportName.iceskating, "Ice Skating");
		sportBase.put(SportName.volleyball, "Volleyball");
		sportBase.put(SportName.running, "Running");
		sportBase.put(SportName.bowling, "Bowling");
		sportBase.put(SportName.futsal, "Futsal");
		sportBase.put(SportName.judo, "Judo");
		sportBase.put(SportName.boxing, "Boxing");
		sportBase.put(SportName.tennis, "Tennis");
		sportBase.put(SportName.curling, "Curling");
		sportBase.put(SportName.kickboxing, "Kickboxing");
		sportBase.put(SportName.jujitsu, "Jujitsu");
		sportBase.put(SportName.biathlon, "Biathlon");
		sportBase.put(SportName.shootingsport, "Shooting");
		sportBase.put(SportName.hockey, "Hockey");
		sportBase.put(SportName.cycling, "Cycling");
		sportBase.put(SportName.archery, "Archery");
		sportBase.put(SportName.darts, "Darts");
		//sportBase.put(SportName., "");
		
		
	}
}
