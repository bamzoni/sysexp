package com.sample;

public class Answer {
	private Question question; 
	private Object reply;
	private boolean acknowledged;
	private boolean questionretracted;
	
	
	public boolean isQuestionretracted() {
		return questionretracted;
	}

	public void setQuestionretracted() {
		this.questionretracted = true;
	}

	public boolean isAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged() {
		this.acknowledged = true;
		System.out.println("Acknowledged");
	}

	public Answer(Question question, Object reply){
		this.question = question;
		this.reply = reply;
		this.acknowledged = false;
		this.questionretracted = false;
		
		System.out.println("new answer : "+ (String)reply + " to question about "+ question.getQuestionType());
	}
	
	public QuestionType getType(){
		return question.getQuestionType();
	}
	
	public Question getQuestion() {
		return question;
	}
	
	public Object getReply() {
		return reply;
	}
	

	
}
