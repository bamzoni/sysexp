package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.drools.runtime.StatefulKnowledgeSession;

import com.sample.Answer;
import com.sample.Question;
import com.sample.QuestionTextBase;

import javax.swing.JTextPane;

public class DecisionDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();

	
	/**
	 * Create the dialog.
	 */
	public DecisionDialog(final Question Droolsquestion, Object answers[]) {
		setTitle("Question");
		setResizable(false);
		setBounds(100, 100, 600, 160);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setText(QuestionTextBase.questionBase.get(Droolsquestion.getQuestionType()));
		contentPanel.add(textPane);
		{
			JTextArea question = new JTextArea();
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				for (final Object a : answers){
					JButton Button = new JButton((String)a);
					Button.setActionCommand("OK");
					buttonPane.add(Button);
					getRootPane().setDefaultButton(Button);
					Button.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							
							StatefulKnowledgeSession ksession = MainWindow.getRef().getExp().getKsession();
							ksession.insert(new Answer(Droolsquestion, a));
							ksession.fireAllRules();
							hide();
							
						}
					});
					
				}
				
			}
			
		}
	}

}
