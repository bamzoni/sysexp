package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sample.QuestionTextBase;
import com.sample.SysExp;

import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JFormattedTextField;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private static MainWindow ref;
	private SysExp exp;
	
	
	public SysExp getExp() {
		return exp;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					ref=frame;
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	public MainWindow() {
		setTitle("Sport Finder");
		ref = this;
		QuestionTextBase.initQuestionTextBase();
		//exp = new SysExp();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 147);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JButton StartButton = new JButton("Start");
		contentPane.add(StartButton, BorderLayout.SOUTH);
		StartButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				exp = new SysExp();
				//hide();
				
			}
		});
		
		JFormattedTextField frmtdtxtfldWelcomeToYour = new JFormattedTextField();
		frmtdtxtfldWelcomeToYour.setEditable(false);
		frmtdtxtfldWelcomeToYour.setText("Welcome to your new sport finder. To begin press start button.");
		contentPane.add(frmtdtxtfldWelcomeToYour, BorderLayout.CENTER);
	}
	
	public static MainWindow getRef(){ return ref;}
	
	public static void hey(){ System.out.println("hey");}
}
